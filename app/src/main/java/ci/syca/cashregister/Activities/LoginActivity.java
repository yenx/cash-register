package ci.syca.cashregister.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ci.syca.cashregister.MyApplication;
import ci.syca.cashregister.R;
import ci.syca.cashregister.Utils.Utils;

import static ci.syca.cashregister.Utils.Utils.dismissLoader;
import static ci.syca.cashregister.Utils.Utils.isConnectionAvailable;
import static ci.syca.cashregister.Utils.Utils.loader;
import static ci.syca.cashregister.Utils.Utils.showLoader;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;

    EditText phoneText;
    EditText passwordText;
    Button loginButton;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        phoneText=findViewById(R.id.input_phone);
        passwordText=findViewById(R.id.input_password);
        loginButton=findViewById(R.id.btn_login);
        
        loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (isConnectionAvailable(getApplicationContext())){
                    login();
                }else{
                    Toast.makeText(getBaseContext(), getString(R.string.not_connexion), Toast.LENGTH_LONG).show();
                }
            }
        });
        //overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

    }

    public void login() {
        Log.d(TAG, "Login");

        if (!validate()) {
            return;
        }

        loginButton.setEnabled(false);

        loader(this);
        showLoader();
        String phone = phoneText.getText().toString();
        String password = passwordText.getText().toString();

        final Map<String,String> param = new HashMap<>();
        param.put("login",phone);
        param.put("password",password);
        Utils.api_call(getApplicationContext(),Utils.loginUrl, Request.Method.POST,param, new Utils.ResponseCallback() {
            JSONObject reader;
            final Gson gson = new GsonBuilder().create();
            @Override
            public void OnSuccess(String result) {
                try {
                    reader = new JSONObject(result);
                    if (reader.getString("success").equals("1")){
                        String data = reader.getString("data");
                        /*User user=gson.fromJson(data, User.class);
                        MyApplication.getInstance().getPrefManager().storeUser(user);
                        Log.d("TAG_PAYER",result+" -----"+user.getName());*/
                        onLoginSuccess();
                    }else{
                        if(reader.getString("success").equals("2")){
                            Toast.makeText(getBaseContext(), "Attendre la prochaine prise de service", Toast.LENGTH_LONG).show();
                        }else{
                            onLoginFailed();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                dismissLoader();
            }

            @Override
            public void OnError(String result) {
                dismissLoader();

            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {

                // TODO: Implement successful signup logic here
                // By default we just finish the Activity and log them in automatically
                this.finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        // Disable going back to the MainActivity
        moveTaskToBack(true);
    }

    public void onLoginSuccess() {
        loginButton.setEnabled(true);
        finish();
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), getString(R.string.login_failed_msg), Toast.LENGTH_LONG).show();

        loginButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String phone = phoneText.getText().toString();
        String password = passwordText.getText().toString();

        if (phone.isEmpty() || !Patterns.PHONE.matcher(phone).matches() || phone.length()!=8) {
            phoneText.setError("Entrer un numero valide");
            valid = false;
        } else {
            phoneText.setError(null);
        }

        if (password.isEmpty()) {
            passwordText.setError("Entrer le mot de passe");
            valid = false;
        } else {
            passwordText.setError(null);
        }

        return valid;
    }
}
