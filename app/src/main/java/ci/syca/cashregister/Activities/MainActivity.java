package ci.syca.cashregister.Activities;

import android.app.MediaRouteButton;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ci.syca.cashregister.Adapters.PageAdapter;
import ci.syca.cashregister.Fragments.InscriptionFragment;
import ci.syca.cashregister.Fragments.LoginFragment;
import ci.syca.cashregister.Fragments.PresentationFragment;
import ci.syca.cashregister.Fragments.SignFragment;
import ci.syca.cashregister.R;
import ci.syca.cashregister.Utils.Utils;

public class MainActivity extends AppCompatActivity {

    private static View signLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        signLayout=findViewById(R.id.sign);
        Button btn_login=findViewById(R.id.btn_login);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signLayout.setVisibility(View.GONE);
                LoginFragment fr = new LoginFragment();
                Utils.switchFragmentWithAnimation(R.id.presentation_content, fr, MainActivity.this, Utils.FRAGMENT_TAG, Utils.AnimationType.SLIDE_IN_SLIDE_OUT);
            }
        });
        Button btn_inscription=findViewById(R.id.btn_inscription);
        btn_inscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signLayout.setVisibility(View.GONE);
                InscriptionFragment fr = new InscriptionFragment();
                Utils.switchFragmentWithAnimation(R.id.presentation_content, fr, MainActivity.this, Utils.FRAGMENT_TAG, Utils.AnimationType.SLIDE_IN_SLIDE_OUT);
            }
        });
        PresentationFragment pfr = new PresentationFragment();
        Utils.switchFragmentWithAnimation(R.id.presentation_content, pfr, MainActivity.this, Utils.FRAGMENT_TAG, Utils.AnimationType.SLIDE_IN_SLIDE_OUT);

    }

    public static void visibleSign(){
        signLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
