package ci.syca.cashregister.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ci.syca.cashregister.Fragments.PagesPresentation.PagePresentatationFragment;

public class PageAdapter extends FragmentStatePagerAdapter {

    private int[] colors;

    public PageAdapter(FragmentManager mgr, int[] colors) {
        super(mgr);
        this.colors = colors;
    }

    @Override
    public int getCount() {
        return(5); // 3 - Number of page to show
    }

    @Override
    public Fragment getItem(int position) {

        return(PagePresentatationFragment.newInstance(position, this.colors[position]));
    }
}
