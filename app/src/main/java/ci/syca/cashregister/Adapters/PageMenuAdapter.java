package ci.syca.cashregister.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ci.syca.cashregister.Fragments.PagesMenu.AllFragment;
import ci.syca.cashregister.Fragments.PagesMenu.CategorieFragment;
import ci.syca.cashregister.Fragments.PagesMenu.PanierFragment;

public class PageMenuAdapter extends FragmentStatePagerAdapter {

    private int numOfTabs;

    public PageMenuAdapter(FragmentManager mgr, int numOfTabs) {
        super(mgr);
        this.numOfTabs = numOfTabs;
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new AllFragment();
            case 1:
                return new CategorieFragment();
            case 2:
                return new PanierFragment();
            default:
                return null;
        }
    }
}
