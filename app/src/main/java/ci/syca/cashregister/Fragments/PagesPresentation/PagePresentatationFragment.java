package ci.syca.cashregister.Fragments.PagesPresentation;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import ci.syca.cashregister.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PagePresentatationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PagePresentatationFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String KEY_POSITION="position";
    private static final String KEY_COLOR="color";

    // TODO: Rename and change types of parameters
    private int position;
    private int color;


    public PagePresentatationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param position Parameter 1.
     * @param color Parameter 2.
     * @return A new instance of fragment PagePresentatationFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PagePresentatationFragment newInstance(int position, int color) {
        PagePresentatationFragment fragment = new PagePresentatationFragment();
        Bundle args = new Bundle();
        args.putInt(KEY_POSITION, position);
        args.putInt(KEY_COLOR, color);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            position = getArguments().getInt(KEY_POSITION, -1);
            color = getArguments().getInt(KEY_COLOR, -1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_page_presentatation, container, false);

        FrameLayout rootView=  v.findViewById(R.id.fragment_page_rootview);
        TextView textView= v.findViewById(R.id.fragment_page_title);

        rootView.setBackgroundColor(color);
        textView.setText("Page numéro "+position);

        Log.e(getClass().getSimpleName(), "onCreateView called for fragment number "+position);

        return v;
    }

}
