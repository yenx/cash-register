package ci.syca.cashregister.Utils;

import android.view.View;

/**
 * Created by ADMIN on 14/03/2018.
 */

public interface ClickListener {
    void onClick(View view, int position);

    void onLongClick(View view, int position);
}
