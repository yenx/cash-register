package ci.syca.cashregister.Utils;

import android.content.Context;
import android.content.SharedPreferences;

public class MyPreferenceManager {


    private String TAG = MyPreferenceManager.class.getSimpleName();

    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "ahtutb";

    // All Shared Preferences Keys
    private static final String KEY_USER_PHONE="phone";
    private static final String KEY_USER_NAME="name";
    private static final String KEY_USER_EMAIL="email";
    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_INTRO = "intro";


    // Constructor
    public MyPreferenceManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }


    /*public void storeUser(User user) {
        editor.putString(KEY_USER_ID, String.valueOf(user.getId()));
        editor.putString(KEY_USER_EMAIL, user.getEmail());
        editor.putString(KEY_USER_NAME, user.getName());
        editor.putString(KEY_USER_PHONE, user.getPhone());
        editor.commit();

        Log.e(TAG, "User is stored in shared preferences. " + user.getEmail() + ", " + user.getId());
    }*/

    public void updateName(String newName){
        editor.putString(KEY_USER_NAME, newName);
        editor.commit();
    }

    public void clearLogin(){
        editor.putString(KEY_USER_ID, null);
        editor.commit();
    }

    public String getEmail() {

        if (pref.getString(KEY_USER_EMAIL, null) != null) {
            return pref.getString(KEY_USER_EMAIL, null);
        }else{
            return null;
        }
    }

    public String getIdUser() {

        if (pref.getString(KEY_USER_ID, null) != null) {
            return pref.getString(KEY_USER_ID, null);
        }else{
            return null;
        }
    }

    public String getUserName() {

        if (pref.getString(KEY_USER_NAME, null) != null) {
            return pref.getString(KEY_USER_NAME, null);
        }else{
            return null;
        }
    }

    public String getUserPhone() {

        if (pref.getString(KEY_USER_PHONE, null) != null) {
            return pref.getString(KEY_USER_PHONE, null);
        }else{
            return null;
        }
    }


    public void storeIntro() {
        editor.putString(KEY_INTRO, "1");
        editor.commit();
    }

    public String getIntro(){
        if (pref.getString(KEY_INTRO, null) != null) {
            return pref.getString(KEY_INTRO, null);
        }else{
            return null;
        }
    }


    public void clear() {
        editor.clear();
        editor.commit();
    }
}
