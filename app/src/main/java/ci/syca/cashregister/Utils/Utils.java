/*
 * Copyright (c) 2017. http://hiteshsahu.com- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * If you use or distribute this project then you MUST ADD A COPY OF LICENCE
 * along with the project.
 *  Written by Hitesh Sahu <hiteshkrsahu@Gmail.com>, 2017.
 */

package ci.syca.cashregister.Utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import ci.syca.cashregister.Activities.MainActivity;
import ci.syca.cashregister.R;

public class Utils {

    public static final String FRAGMENT_TAG = "fragment";
    public static String user_mdp;

    public  static String SUCCES="1";
    //public static final String baseUrl="http://192.168.1.73/ahtutb/api/service/";
    public static final String baseUrl = "http://transport.sycapay.net/ahtutb/index.php/api/service/";
    public static final String wsUrl = "http://topup.sycapay.com/";
    public static final String sendSMS = wsUrl+"api/Sms/setSms/";
    public static final String departsUrl=baseUrl+"departs/";
    public static final String destinationsUrl=baseUrl+"destinations/";
    public static final String dteTimeUrl=baseUrl+"dtetime_depart/";
    public static final String ligneDetailsUrl=baseUrl+"ligne_details/";
    public static final String addBookingUrl=baseUrl+"add_booking/";
    public static final String bookingHistoriqueUrl=baseUrl+"booking_historiques/";
    public static final String loginUrl=baseUrl+"login/";
    public static final String getClotureUrl=baseUrl+"get_cloture_user/";
    public static final String lastDateClotureUserUrl=baseUrl+"last_date_cloture_user/";
    public static final String addClotureUrl=baseUrl+"add_cloture_user/";
    public static final String bookingHistoriqueUserUrl=baseUrl+"booking_historiques_user/";
    public static final String updateUsernameUrl=baseUrl+"update_username/";
    public static final String bookingClotureUrl=baseUrl+"booking_cloture/";


    public static String ALTERNATIVE=null;

    public static boolean isConnectionAvailable(Context context) {

        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnected()
                    && netInfo.isConnectedOrConnecting()
                    && netInfo.isAvailable()) {
                return true;
            }
        }
        return false;
    }

    public static String getIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = enumNetworkInterfaces
                        .nextElement();
                Enumeration<InetAddress> enumInetAddress = networkInterface
                        .getInetAddresses();
                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress.nextElement();

                    if (inetAddress.isSiteLocalAddress()) {
                        ip += "SiteLocalAddress: "
                                + inetAddress.getHostAddress() + "\n";
                    }
                }
            }
        } catch (SocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ip += "Something Wrong! " + e.toString() + "\n";
        }
        return ip;
    }


    /**
     * Return date in specified format.
     * @param milliSeconds Date in milliseconds
     * @param dateFormat Date format
     * @return String representing date in specified format
     */
    public static String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static Long getDate(String format,String dte){
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        Date date = null;
        try {
            date = formatter.parse(dte);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long mills = date.getTime();
        return mills;
    }


    public static Map<String, String> tags = new HashMap<>();

    public static final String RESERVATION_FRAGMENT_TAG = "ReservationFragment";
    public static final String RESERVATION_DETAIL_FRAGMENT_TAG = "ReservationDetailFragment";


    private static String CURRENT_TAG = null;
    private static Map<String, Typeface> TYPEFACE = new HashMap<String, Typeface>();
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    public static int REQUEST_WEBVIEW_CODE = 95;

    static ProgressDialog progressDialog;

    public static int RECHARGE_STL = 11;
    public static int RECHARGE_AQUA = 14;
    public static int RECHARGE_SOTRA = 17;
    public static int RECHARGE_METRO = 20;


    public static final String[] moyens_paiement = {"Mobile Money","Carte Bancaire", "Wallet"};
    public static final String[] numeros_orange = {"7", "8", "9"};

    public static final class Type_paiement {

        public static final String WALLET = "WALLET";
        public static final String VISA = "VISA";
        public static final String MOBILE_MONEY = "MOBILE MONEY";

    }



    public static String getUser_mdp() {
        return user_mdp;
    }

    public static void setUser_mdp(String user_mdp) {
        Utils.user_mdp = user_mdp;
    }

    public static int getToolbarHeight(Context context) {
        int height = (int) context.getResources().getDimension(
                R.dimen.abc_action_bar_default_height_material);
        return height;
    }

    public static void loader(Context context){
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Chargement..."); // Setting Message
//        progressDialog.setTitle("ProgressDialog"); // Setting Title
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.setCancelable(false);
    }

    public static void showLoader(){
        progressDialog.show();
    }

    public static void dismissLoader(){
        progressDialog.dismiss();
    }

    public static Drawable tintMyDrawable(Drawable drawable, int color) {
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, color);
        DrawableCompat.setTintMode(drawable, PorterDuff.Mode.SRC_IN);
        return drawable;
    }

    public static String getRealPathFromURI(Uri contentUri, Context mContext) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(mContext, contentUri, proj,
                null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

    /**
     * Convert milliseconds into time hh:mm:ss
     *
     * @param milliseconds
     * @return time in String
     */
    public static String getDuration(long milliseconds) {
        long sec = (milliseconds / 1000) % 60;
        long min = (milliseconds / (60 * 1000)) % 60;
        long hour = milliseconds / (60 * 60 * 1000);

        String s = (sec < 10) ? "0" + sec : "" + sec;
        String m = (min < 10) ? "0" + min : "" + min;
        String h = "" + hour;

        String time = "";
        if (hour > 0) {
            time = h + ":" + m + ":" + s;
        } else {
            time = m + ":" + s;
        }
        return time;
    }

    public static int dpToPx(Context context, int dp) {
        float density = context.getResources().getDisplayMetrics().density;
        return Math.round((float) dp * density);
    }

    public static void switchFragmentWithAnimation(int id, Fragment fragment, FragmentActivity activity, String TAG, AnimationType transitionStyle) {

        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();

        if (transitionStyle != null) {
            switch (transitionStyle) {
                case SLIDE_DOWN:

                    // Exit from down
                    fragmentTransaction.setCustomAnimations(R.anim.slide_up,
                            R.anim.slide_down);

                    break;

                case SLIDE_UP:

                    // Enter from Up
                    fragmentTransaction.setCustomAnimations(R.anim.slide_in_up,
                            R.anim.slide_out_up);

                    break;

                case SLIDE_LEFT:

                    // Enter from left
                    fragmentTransaction.setCustomAnimations(R.anim.slide_left,
                            R.anim.slide_out_left);

                    break;

                // Enter from right
                case SLIDE_RIGHT:
                    fragmentTransaction.setCustomAnimations(R.anim.slide_right,
                            R.anim.slide_out_right);

                    break;

                case FADE_IN:
                    fragmentTransaction.setCustomAnimations(R.anim.fade_in,
                            R.anim.fade_out);

                case FADE_OUT:
                    fragmentTransaction.setCustomAnimations(R.anim.fade_in,
                            R.anim.donot_move);

                    break;

                case SLIDE_IN_SLIDE_OUT:

                    fragmentTransaction.setCustomAnimations(R.anim.slide_left,
                            R.anim.slide_out_left);

                    break;

                default:
                    break;
            }
        }

        CURRENT_TAG = TAG;

        fragmentTransaction.replace(id, fragment);
        fragmentTransaction.addToBackStack(TAG);
        fragmentTransaction.commit();
    }

    public static void switchFragmentWithAnimationAnddata(int id, Fragment fragment, FragmentActivity activity, String TAG, AnimationType transitionStyle, String nonPage) {

        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();

        //using Bundle to send data
        Bundle bundle=new Bundle();
        bundle.putString("page",nonPage);
        fragment.setArguments(bundle); //data being send to SecondFragment

        if (transitionStyle != null) {
            switch (transitionStyle) {
                case SLIDE_DOWN:

                    // Exit from down
                    fragmentTransaction.setCustomAnimations(R.anim.slide_up,
                            R.anim.slide_down);

                    break;

                case SLIDE_UP:

                    // Enter from Up
                    fragmentTransaction.setCustomAnimations(R.anim.slide_in_up,
                            R.anim.slide_out_up);

                    break;

                case SLIDE_LEFT:

                    // Enter from left
                    fragmentTransaction.setCustomAnimations(R.anim.slide_left,
                            R.anim.slide_out_left);

                    break;

                // Enter from right
                case SLIDE_RIGHT:
                    fragmentTransaction.setCustomAnimations(R.anim.slide_right,
                            R.anim.slide_out_right);

                    break;

                case FADE_IN:
                    fragmentTransaction.setCustomAnimations(R.anim.fade_in,
                            R.anim.fade_out);

                case FADE_OUT:
                    fragmentTransaction.setCustomAnimations(R.anim.fade_in,
                            R.anim.donot_move);

                    break;

                case SLIDE_IN_SLIDE_OUT:

                    fragmentTransaction.setCustomAnimations(R.anim.slide_left,
                            R.anim.slide_out_left);

                    break;

                default:
                    break;
            }
        }

        CURRENT_TAG = TAG;

        fragmentTransaction.replace(id, fragment);
        fragmentTransaction.addToBackStack(TAG);
        fragmentTransaction.commit();
    }

    public static void switchFragmentWithAnimation(FragmentManager fragManager, int id, Fragment fragment, FragmentActivity activity, String TAG, AnimationType transitionStyle) {

//        FragmentManager fragmentManager = fragManager;
        FragmentTransaction fragmentTransaction = fragManager
                .beginTransaction();

        if (transitionStyle != null) {
            switch (transitionStyle) {
                case SLIDE_DOWN:

                    // Exit from down
                    fragmentTransaction.setCustomAnimations(R.anim.slide_up,
                            R.anim.slide_down);

                    break;

                case SLIDE_UP:

                    // Enter from Up
                    fragmentTransaction.setCustomAnimations(R.anim.slide_in_up,
                            R.anim.slide_out_up);

                    break;

                case SLIDE_LEFT:

                    // Enter from left
                    fragmentTransaction.setCustomAnimations(R.anim.slide_left,
                            R.anim.slide_out_left);

                    break;

                // Enter from right
                case SLIDE_RIGHT:
                    fragmentTransaction.setCustomAnimations(R.anim.slide_right,
                            R.anim.slide_out_right);

                    break;

                case FADE_IN:
                    fragmentTransaction.setCustomAnimations(R.anim.fade_in,
                            R.anim.fade_out);

                case FADE_OUT:
                    fragmentTransaction.setCustomAnimations(R.anim.fade_in,
                            R.anim.donot_move);

                    break;

                case SLIDE_IN_SLIDE_OUT:

                    fragmentTransaction.setCustomAnimations(R.anim.slide_left,
                            R.anim.slide_out_left);

                    break;

                default:
                    break;
            }
        }

        CURRENT_TAG = TAG;

        fragmentTransaction.replace(id, fragment);
        fragmentTransaction.addToBackStack(TAG);
        fragmentTransaction.commit();
    }

    public static AlertDialog showAlert(final Context context, String title, String msg) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

        dialogBuilder.setTitle(title)
                    .setMessage(msg)
                    .setCancelable(false);

        dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                //do something with edt.getText().toString();
            }
        });

        return dialogBuilder.create();
    }

    public static AlertDialog choiceAlert(final Context context, String title, String msg) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

        dialogBuilder.setTitle(title)
                .setMessage(msg)
                .setCancelable(false);

        dialogBuilder.setPositiveButton("Confirmer", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                //do something with edt.getText().toString();
            }
        });

        dialogBuilder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });

        return dialogBuilder.create();
    }
/*

    public static AlertDialog saisieMobile(final Context context, LayoutInflater layoutInflater) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        final View dialogView = layoutInflater.inflate(R.layout.dialog_numero, null);
        dialogBuilder.setView(dialogView);


        dialogBuilder.setTitle("Entrez votre numéro mobile");
//        dialogBuilder.setMessage("Mot de passe pour confirmation");
        dialogBuilder.setPositiveButton("Confirmer", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                //do something with edt.getText().toString();
            }
        });
        dialogBuilder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        return dialogBuilder.create();
    }
*/

    public enum AnimationType {
        SLIDE_LEFT, SLIDE_RIGHT, SLIDE_UP, SLIDE_DOWN, FADE_IN, SLIDE_IN_SLIDE_OUT, FADE_OUT
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkPermission(final Context context){
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if(currentAPIVersion>= Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_CONTACTS)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission nécéssaire");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_CONTACTS}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public static void api_call(final Context context, String url, Integer Methode, final Map<String,String> data, final ResponseCallback responseCallback){

        RequestQueue queue = AppSingleton.getInstance(context).getRequestQueue();
//        RequestQueue requestQueue = Volley.newRequestQueue(context);

        StringRequest stringRequest = new StringRequest(Methode, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (responseCallback != null)
                    responseCallback.OnSuccess(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Mysmart",""+error);
                //Toast.makeText(context, ""+error, Toast.LENGTH_SHORT).show();
                try{

                    /*if (context.getClass().getSimpleName().equals("QrPaiementActivity"))
                        ((MainActivity)context).getProgressDialog().dismiss();
                    else
                        ((MainActivity)context).getProgressDialog().dismiss();*/

                } catch (Exception e){
                    e.printStackTrace();
                }
                dismissLoader();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> param = new HashMap<>();
                param = data;
                return param;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<>();
                headers.put("X-API-KEY", "@@@@");
                return headers;
            }

        };

        AppSingleton.getInstance(context).addToRequestQueue(stringRequest);

    }

    public static boolean isformValid(ArrayList<EditText> editTexts){
        boolean isValid = true;
        for (EditText editText : editTexts) {
            if (editText.getText().toString().isEmpty()){
                isValid = false;
                editText.setError("Ce champ ne doit pas être vide");
            }
        }

        if (isValid)
            return true;
        else
            return false;
    }

    public static boolean arePasswordsConformed(EditText et_password, EditText et_confirm_password){
        boolean isValid = true;

        if (et_password.getText().toString().equals(et_confirm_password.getText().toString())){
            isValid = true;
        } else {
            isValid = false;
            et_password.setError("Les mots de passes de sont pas conformes");
            et_confirm_password.setError("Les mots de passes de sont pas conformes");
        }

        if (isValid)
            return true;
        else
            return false;
    }

    public interface ResponseCallback {

        public void OnSuccess(String result);

        public void OnError(String result);

    }

    public static void sendNotification(Context context, String titre, String contenu, String channelId, int frs_id, String frs_name, String service, String ss_service, String lib_details, String cout_details, String date_achat, String date_validite) {

        Random r = new Random();
        // Create an explicit intent for an Activity in your app
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("fragmentDetailsInbox", "InboxDetailsActivityFragment");
        intent.putExtra("frs_id", frs_id);
        intent.putExtra("frs_name", frs_name);
        intent.putExtra("service", service);
        intent.putExtra("ss_service", ss_service);
        intent.putExtra("lib_details", lib_details);
        intent.putExtra("cout_details", cout_details);
        intent.putExtra("date_achat", date_achat);
        intent.putExtra("date_validite", date_validite);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        //Get an instance of NotificationManager//

        String GROUP_KEY = "com.sycapay.net";
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context, channelId)
                        .setSmallIcon(R.drawable.ic_launcher_foreground)
                        .setContentTitle(titre)
                        .setContentText(contenu)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(contenu))
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        // Set the intent that will fire when the user taps the notification
                        .setContentIntent(pendingIntent)
                        .setVisibility(Notification.VISIBILITY_PRIVATE)
                        .setGroup(GROUP_KEY)
                        .setAutoCancel(true);


        // Gets an instance of the NotificationManager service//

        NotificationManagerCompat mNotificationManager = NotificationManagerCompat.from(context);

        // When you issue multiple notifications about the same type of event,
        // it’s best practice for your app to try to update an existing notification
        // with this new information, rather than immediately creating a new notification.
        // If you want to update this notification at a later date, you need to assign it an ID.
        // You can then use this ID whenever you issue a subsequent notification.
        // If the previous notification is still visible, the system will update this existing notification,
        // rather than create a new one. In this example, the notification’s ID is 001//

                mNotificationManager.notify(r.nextInt(), mBuilder.build());
    }

    public static void sendNotification(Context context, String titre, String contenu, String channelId) {

        Random r = new Random();
        // Create an explicit intent for an Activity in your app
        Intent intent = new Intent(context, MainActivity.class);

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        //Get an instance of NotificationManager//

        String GROUP_KEY = "com.sycapay.net";
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context, channelId)
                        .setSmallIcon(R.drawable.ic_launcher_foreground)
                        .setContentTitle(titre)
                        .setContentText(contenu)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(contenu))
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        // Set the intent that will fire when the user taps the notification
                        .setContentIntent(pendingIntent)
                        .setVisibility(Notification.VISIBILITY_PRIVATE)
                        .setGroup(GROUP_KEY)
                        .setAutoCancel(true);


        // Gets an instance of the NotificationManager service//

        NotificationManagerCompat mNotificationManager = NotificationManagerCompat.from(context);

        // When you issue multiple notifications about the same type of event,
        // it’s best practice for your app to try to update an existing notification
        // with this new information, rather than immediately creating a new notification.
        // If you want to update this notification at a later date, you need to assign it an ID.
        // You can then use this ID whenever you issue a subsequent notification.
        // If the previous notification is still visible, the system will update this existing notification,
        // rather than create a new one. In this example, the notification’s ID is 001//

                mNotificationManager.notify(r.nextInt(), mBuilder.build());
    }

    private static void logUser(String mobile, String Nom, String Prenoms) {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        //Crashlytics.setUserIdentifier(mobile);
//        Crashlytics.setUserEmail("user@fabric.io");
        //Crashlytics.setUserName(Nom + " " + Prenoms);
    }

    public static String firstTwo(String str) {
        return str.length() < 2 ? str : str.substring(1, 2);
    }

    public static boolean checkIfExists(String[] myStringArray, String stringToLocate) {
        for (String element:myStringArray ) {
            if ( element.equals( stringToLocate)) {
                return true;
            }
        }
        return false;
    }
}
